// pages/manage/nav/index.js
var wxCharts = require('../../../utils/wxcharts.js');
var app = getApp();
var lineChart = null;

Page({
data: {
    width:180,
    height:400,
    diyProduct:0
  },
  touchHandler: function (e) {
    console.log(lineChart.getCurrentDataIndex(e));
    lineChart.showToolTip(e, {
      // background: '#7cb5ec',
      format: function (item, category) {
        return category + ' ' + item.name + ':' + item.data
      }
    });
  },
  createSimulationData: function () {
    var categories = [];
    var data = [];
    for (var i = 0; i < 10; i++) {
      categories.push('2016-' + (i + 1));
      data.push(Math.random() * (20 - 10) + 10);
    }
    // data[4] = null;
    return {
      categories: categories,
      data: data
    }
  },
  onLoad: function (e) {
    console.log(1111111111)
    this.setData({
      diyProduct:app.globalData.diyProduct
    })
    console.log(this.data.diyProduct);

    var windowWidth = 320;
    try {
      var res = wx.getSystemInfoSync();
      windowWidth = res.windowWidth-20;
      this.setData({
        width:res.windowWidth,
        height:res.windowHeight
      })
      console.log(windowWidth)
    } catch (e) {
      console.error('getSystemInfoSync failed!');
    }

    var simulationData = this.createSimulationData();
    console.log(simulationData);
    lineChart = new wxCharts({
      canvasId: 'lineCanvas',
      type: 'line',
      categories: simulationData.categories,
      animation: true,
      // background: '#f5f5f5',
      series: [{
        name: '总用户数',
        data: simulationData.data,
        format: function (val, name) {
          return val.toFixed(2) + '万';
        }
      }, {
        name: '成交量2',
        data: [2, 0, 0, 3, null, 4, 0, 0, 2, 0],
        format: function (val, name) {
          return val.toFixed(2) + '万';
        }
      }],
      xAxis: {
        disableGrid: true
      },
      yAxis: {
        title: '成交金额 (万元)',
        format: function (val) {
          return val.toFixed(2);
        },
        min: 0
      },
      width: windowWidth,
      height: 160,
      dataLabel: false,
      dataPointShape: true,
      extra: {
        lineStyle: 'curve'
      }
    });
  },
  productMan:function(e) {
    wx.navigateTo({
      url: '../product-man/index'
    })
  },
  orderMan: function (e) {
    wx.navigateTo({
      url: '../order-man/index'
    })
  },
  userMan: function (e) {
    wx.navigateTo({
      url: '../user-man/index'
    })
  },
  productAna: function (e) {
    wx.navigateTo({
      url: '../product-ana/index'
    })
  },
  orderAna: function (e) {
    wx.navigateTo({
      url: '../order-ana/index'
    })
  },
  userAna: function (e) {
    wx.navigateTo({
      url: '../user-ana/index'
    })
  },
  goDiy:function(e){
    wx.navigateTo({
      url: '/pages/dy/index/index'
    })
  },
  couponMan: function (e) {
    wx.navigateTo({
      url: '../coupon-man/index'
    })
  },
})