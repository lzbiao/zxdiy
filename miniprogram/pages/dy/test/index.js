import Toast from '../../../components/dist/toast/toast';
const util = require("../../../utils/util.js");
const db = wx.cloud.database();
var app = getApp();
const icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAYWSURBVGje7ZhtkJZVGcd/9y4E64IMtEO4EyKhaBKTbPDBdCmHbJWMpBEIWYc1X5dxGrEJexFiJouYabYpFNNmdgYXmtpBZHwZqcbRQKIpNxuxHFNwaiZGhBSBD0rprw/3ee7n3A/Ps89LTX1ory/3uf/n5fqf65zrOtc5MCIjMiL/75JUb2InnXTwQUbVPfpxXmIfv0r+0iABp7KeL4afY/wTgDaOljSrjEykOSA9PJhYJ31vU7XfuRF2pXplrlW/2pZDdqgTsr8WV3pKPeWsOixgwgPcyP4yVbNPQ2tBYDZwWfJ0rbO/2z/7n5bfqR+uTf3FWafOHD7OvoA/4w2eny1BAn7UL3kw65ezrB0Z/qbN1dUnHlZ1IE/B7jDIdTaV7IFMnW1+LbRaWKK+R92kXlOdwEXqenXAyQUKjvNxVfvU9lzr/vx8JZvtDsdn6pdCIHAk7wxNZRhcB2wBSF7nA8BuOznEQn7KuBq3EJzJAIs5bgdDwKJkMOCP08aUahY4qTapAwDBCroaoFYLALgk9PxUqNFNfkG9vJoFWnkheS/7eycEoLdrnn1BDoTvyQj7I3BhNQLwSjafhJ2M4uvAZntLLDXPte5lJXDMx7zBibna1PirgH1OzeBjQDvDi/ozSJfAm9RnTMJW6k2XwAmuL+vp+5wTNmFoD3apB2wOS9Cu9tVMwLNUnZzOKPOCHlUPeI2jC6HYUS72N6r+OKMTLOZ31JsaIzCYOlDBqNFcL83Q6CzwPHeXqgfHqNqqbrK7lEBSjkC13RXJZp7nH0xnGefV2GOI3ckdxd/yZ/xgskzZSjd35vBFXALAncBGAGbSwvVsC+q/y5sBP8j9uZ4peg8b+Bu7a1gCJ6n6SmwMr1VfjpZhpUm6BABe4onchrwtN+bzWn4PNA3LZV1xhRzLNuBRYBU/B1YlW+IUI9nLDGAbTwZgk2dGI327korhCTwVlRcCOwHYTBenxQUncxhoZQEAnwWWRdVPN0bgcFReC2wI5Uv5WJ5CUD+fHuAo8EtgY2Sg1xshcLAYkG3lIuAPwP28yN7k9zGFgvpkT/IWtwPwDoNMZFKhfyJP1E/gT1H5bGB/cgo4yN0JUKCQWWp+sgeA7aHHI8DMaIQ99RFYShq3CzKd4o4YCrNKKVwPkXp4DYBbGQ+52PAyAIuoLlUyuzVWkyMeH6b22bwbDheIfpIz232s4wgzgd4cmkqMfYvx9AL30Zv8KJtWF7vqDUS/iLDx6hawzzWF0yGkKv1hZiF3dIpHFFyhfiYaYXldgSh5A+iIgBPACgE+xFdS9cHxgCxxi1d5EfltXCEhr0DAScD7fV9GCO6lmWnALcx1TtHxAHivQMEz0jPAMSwF/hoNeVVdBIKcE5X7Ifg4DOXUU0xf+T7QBlwOrEvezSY0ljmNEFgclZ/jRCCwiiSvPqLQGs6CRyluUIB51C7RaWh8j3GB+lLkUJ+XYkJiR+6k1C/nxtxV6TSsdOe/EdhKN5/MTjeSJ93J1UAhH3gIfILXgO+5EojzgVdpdk00Xlf4dpcq+p9nRMMtwYCr1U9keJwTLs/Q/iLhCjnh2ap2N5KUtqg6JlJfzIr1ZicUCERZ8eY8BRN/q37TKXURSC0Azld/kKnvrHIveMgLKL0XpO8sLfUReLhAAPyq2lsItvHdML0Z+a76oj/0Cov9zSinPedBIDBV3VidwP6IQOJgMdZXv5xSvJwW9kwPZARmq7fHrcsHoo9E5QtZAsAdjqU+OSN8WyJsFukFdVgCW4HwyuW5vEB6xbyav9f4wgOIq9kDrCCfvnZD2aevXOfLLLyQTMu20jkezbyghiXwbfUNp4XbhPaGJdC3qoYZR4e1G4j92SbXBfwBz61EwLO8K7TaYIiyGYWUwPJq+gGXnh5OAJzhUwE/6V1eXCTgBD/nvZFDzsj1uzaqGZ3XVfahUthFF3CoTGW154VDtJft2c6zzGVuMlQDAbCV/Uyv8FLamPyaj7Mk2V5ze1vcHnK++K24r/Sois+CgOyIkeytWBeU0zP8a/mneTjz5n/vtfwe1ibHGrKcs/yGz9monHCbi21qSPWIjMiI/HfkXwSZaWJJZaXhAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE3LTA0LTA0VDExOjQ3OjQ1KzA4OjAwI6N5UAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNy0wNC0wNFQxMTo0Nzo0NSswODowMFL+wewAAAAASUVORK5CYII='
const buttons = [{
  label: '组件',
  icon,
},
{
  label: '页面',
  icon,
},
{
  label: '保存',
  icon,
},
{
  label: '跳转',
  icon,
},
]

Page({
  data: {
    types: ['topLeft', 'topRight', 'bottomLeft', 'bottomRight', 'center'],
    typeIndex: 3,
    colors: ['light', 'stable', 'positive', 'calm', 'balanced', 'energized', 'assertive', 'royal', 'dark'],
    colorIndex: 6,
    dirs: ['horizontal', 'vertical', 'circle'],
    dirIndex: 1,
    sAngle: 0,
    eAngle: 360,
    spaceBetween: 10,
    buttons,
    color: wx.getExtConfigSync().color,
    value: '',
    files: [],
    uploadImgCount: 0,
    uploadImgArry: [],
    background: ['demo-text-1', 'demo-text-2', 'demo-text-3'],
    vertical: false,
    tmpIconNum: 3,
    show: {
      middle: false,
      top: false,
      bottom: false,
      right: false,
      right2: false
    },
    show_num: 0,
    show_type: '',
    diy: [],
    goodSelected: '',
    tmpGoodsImgs: '',
    tmpCurrentPageName: '',
    tmpCurrentPageI: 0,
    tmpAddPageName: '',
    tmpIconSelectIndex: 0,
    all: [],
    pageType: '',
    showPageType: false,
    showPageAdd: false,
    actions: [
      {
        name: '一级页面'
      },
      {
        name: '二级页面'
      },
      {
        name: '三级页面'
      }
    ]

  },
  onChange(e) {
    this.setData({
      value: e.detail
    });
  },
  onLoad: function () {
    db.collection('index_diy').orderBy('createtime', 'desc').limit(1)
      .get().then(res => {
        console.log(res)
        if (res.data.length > 0) {
          this.setData({
            all: res.data[0].all,
            tmpCurrentPageName: '首页'
            // tmpCurrentPageName: wx.getStorageSync('tmpCurrentPageName')
          })
          this.getPageData()
        } else {
          let d = { name: "首页", content: [] }
          let a = this.data.all
          a.push(d)
          let zy = { name: "主页", content: [], url: "../../../pages/com/index/index" }
          let glzx = { name: "管理中心", content: [], url: "../../../pages/manage/nav/index" }
          let splb = { name: "商品列表", content: [], url: "../../../pages/shop/product/product-list/index" }
          // let yhq = { name: "优惠券", content: [], url: "../../../pages/shop/coupons/index" }
          // let mdiy = {name:"DIY",content:[],url:"../../../pages/dy/test/index"}
          a.push(zy)
          a.push(glzx)
          a.push(splb)
          // a.push(yhq)
          // a.push(mdiy)
          this.setData({
            tmpCurrentPageName: '首页',
            all: a
          })
        }

      })
    wx.setNavigationBarTitle({
      title: "首页",
    })
  },
  onShow: function () {
    var that = this;
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];
    if (currPage.data.goodSelected) {
      that.setData({
        goodSelected: currPage.data.goodSelected
      })
      let goodsDiy = that.data.diy
      goodsDiy[that.data.show_num].product_list[parseInt(that.data.tmpGoodsImgs)] = that.data.goodSelected
      let a = that.data.all
      a[that.data.tmpCurrentPageI].content = goodsDiy
      that.setData({
        diy: goodsDiy,
        all: a

      })

    }
  },
  onDiyAdd(e) {
    this.toggle('right');
    let tmpDiy = this.data.diy;
    let type = e.currentTarget.dataset.index;
    console.log(type)
    let mSearch = { type: 'search' };
    let mSwiper = {
      type: 'swiper', imageUrl: ['../../../images/noimg2.png'], indicatorDots: true, autoplay: false, duration: 500, interval: 2000, swiperHieght: 200
    };
    let mMenu = { type: 'menu', icon: [{ url: '../../../images/demoicon.png', title: '图标1', event: '' }, { url: '../../../images/demoicon.png', title: '图标2', event: '' }, { url: '../../../images/demoicon.png', title: '图标3', event: '' }] };
    let mText = { type: 'text', context: '我是文本', layoutLeft: 30 };
    let mImage = { type: 'image', url: '../../../images/noimg2.png', imageHieght: 200 };
    let mGoods = { type: 'goods', show_type: 1, product_list: [{ id: '1', img_url: '/images/noimg2.png', name: '商品', price: '0.2', price_org: '0.1', sale: '10', subname: '描述' }] };
    if (type == 'search') {
      tmpDiy.push(mSearch);
    } else if (type == 'swiper') {
      tmpDiy.push(mSwiper);
    } else if (type == 'menu') {
      tmpDiy.push(mMenu);
    } else if (type == 'text') {
      tmpDiy.push(mText);
    } else if (type == 'image') {
      tmpDiy.push(mImage);
    } else if (type == 'goods') {
      tmpDiy.push(mGoods);
    }
    let tmpDiyD = { name: this.data.tmpCurrentPage, content: tmpDiy };
    let tmpAll = this.data.all
    // tmpAll[this.data.tmpCurrentPageI].name = this.data.tmpCurrentPageName
    tmpAll[this.data.tmpCurrentPageI].content = tmpDiy
    console.log(tmpDiy)
    this.setData({
      diy: tmpDiy,
      all: tmpAll
    });
  },
  getPageData: function (e) {
    for (var i = 0; i < this.data.all.length; i++) {
      if (this.data.tmpCurrentPageName == this.data.all[i].name) {
        this.setData({
          diy: this.data.all[i].content,
          tmpCurrentPageI: i
        })
      }
    }
  },
  onDiyEdit(e) {
    if (e.currentTarget.dataset.type == 'swiper') {
      if (this.data.diy[e.currentTarget.dataset.index].imageUrl[0] == '../../../images/noimg2.png')
        this.setData({
          files: []
        })
      else
        this.setData({
          files: this.data.diy[e.currentTarget.dataset.index].imageUrl
        })
    } else if (e.currentTarget.dataset.type == 'menu') {
      // this.setData({
      //   tmpIconNum :
      // })
    } else if (e.currentTarget.dataset.type == 'text') {
      // this.setData({
      //   tmpIconNum :
      // })
    }
    this.toggle('bottom');
    this.setData({
      show_num: e.currentTarget.dataset.index,
      show_type: e.currentTarget.dataset.type
    })
  },
  onDiyDelete(e) {
    let tt = this.data.diy;
    tt.splice(e.currentTarget.dataset.index, 1)
    this.setData({
      diy: tt
    })
  },
  onDiyMoveUp(e) {

  },
  onDiyMoveDown(e) {

  },

  onSearch(event) {
    if (this.data.value) {
      // wx.showToast({
      //   title: '搜索：' + this.data.value,
      //   icon: 'none'
      // });
      wx.navigateTo({
        url: '../../../pages/shop/product/product-list/index?text=' + this.data.value,
      })
    }
  },

  onCancel() {
    wx.showToast({
      title: '取消',
      icon: 'none'
    });
  },

  onClear() {
    wx.showToast({
      title: '清空',
      icon: 'none'
    });
  },
  changeIndicatorDots: function (e) {
    let ui = this.data.diy;
    ui[this.data.show_num].indicatorDots = e.detail.value
    this.setData({
      diy: ui
    })
  },
  changeAutoplay: function (e) {
    let ui = this.data.diy;
    ui[this.data.show_num].autoplay = e.detail.value;
    this.setData({
      diy: ui
    })
  },
  intervalChange: function (e) {
    let ui = this.data.diy;
    ui[this.data.show_num].interval = e.detail.value

    this.setData({
      diy: ui
    })

  },
  durationChange: function (e) {
    let ui = this.data.diy;
    ui[this.data.show_num].duration = e.detail.value

    this.setData({
      diy: ui
    })
  },
  swiperHeightChange: function (e) {
    let ui = this.data.diy;
    ui[this.data.show_num].swiperHieght = e.detail.value

    this.setData({
      diy: ui
    })
  },
  imageHeightChange: function (e) {
    let ui = this.data.diy;
    ui[this.data.show_num].imageHieght = e.detail.value

    this.setData({
      diy: ui
    })
  },
  addComponent: function (e) {
    console.log(123)
  },

  toggle(type) {
    this.setData({
      [`show.${type}`]: !this.data.show[type]
    });
  },

  togglePopup() {
    this.toggle('middle');
  },

  toggleRightPopup() {
    this.toggle('right');
  },

  toggleRightPopup2() {
    this.toggle('right2');
  },

  toggleBottomPopup() {
    this.toggle('bottom');
  },

  toggleTopPopup() {
    this.toggle('top');
    setTimeout(() => {
      this.toggle('top');
    }, 2000);
  },
  chooseImage: function (e) {
    var that = this;
    var images = that.data.files;
    wx.chooseImage({
      count: 6 - images.length,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        console.log(res)
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          files: that.data.files.concat(res.tempFilePaths)
        });
      }
    })
  },
  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },
  deleteImage: function (e) {
    var that = this;
    var images = that.data.files;
    var index = e.currentTarget.dataset.index; //获取当前长按图片下标
    wx.showModal({
      title: '系统提醒',
      content: '确定要删除此图片吗？',
      success: function (res) {
        if (res.confirm) {
          images.splice(index, 1);
          let tt = that.data.diy;
          console.log(tt)
          tt[that.data.show_num].imageUrl.splice(index, 1)
          console.log(tt)
          that.setData({
            diy: tt
          })

        } else if (res.cancel) {
          return false;
        }
        that.setData({
          files: images
        });
      }
    })
  },
  uploadImg: function (e) {
    wx.showLoading({
      title: '数据同步中',
    })

    for (let i = 0; i < this.data.files.length; i++) {
      const filePath = this.data.files[i]
      // 上传图片
      const cloudPath = 'dzimg/' + wx.getStorageSync('openid') + '/swiper/' + (new Date()).valueOf() + filePath.match(/\.[^.]+?$/)[0]
      wx.cloud.uploadFile({
        cloudPath,
        filePath,
        success: res => {
          console.log('[上传文件] 成功：', res)
          let ui = this.data.diy;
          ui[this.data.show_num].imageUrl[i] = res.fileID

          this.setData({
            diy: ui
          })

          //TODO resfileId 到DIY json
          if (i + 1 == this.data.files.length)
            wx.hideLoading();
        },
        fail: e => {
          console.error('[上传文件] 失败：', e)
          wx.showToast({
            icon: 'none',
            title: '上传失败',
          })
          wx.hideLoading();
        },
        complete: () => {
        }
      })

    }

  },
  onMenuChange: function (e) {
    console.log(e.detail)
    console.log(this.data.tmpIconNum)
    let mt = { url: '../../../images/demoicon.png', title: '图标' }
    let menuDiy = this.data.diy

    if (menuDiy[this.data.show_num].icon.length > parseInt(e.detail)) {
      menuDiy[this.data.show_num].icon.splice(menuDiy[this.data.show_num].icon.length - 1, 1)
      this.setData({
        diy: menuDiy
      })

    } else if (menuDiy[this.data.show_num].icon.length < parseInt(e.detail)) {
      console.log(345)
      menuDiy[this.data.show_num].icon.push(mt)
      this.setData({
        diy: menuDiy
      })
    }
    console.log(menuDiy[this.data.show_num].icon)
    // this.setData({
    //   tmpIconNum:e.detail
    // })

  },
  chooseIconImageEvent: function (e) {
    let index = e.currentTarget.dataset.index;
    this.setData({
      tmpIconSelectIndex: index
    })
    this.toggleRightPopup2();
  },
  chooseIconImage: function (e) {
    console.log(e)
    let index = e.currentTarget.dataset.index;
    console.log(index + "aaaaa")
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        console.log(res)
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.uploadIconImg(res.tempFilePaths[0], index)
      }
    })
  },
  uploadIconImg: function (e, i) {
    console.log("qqq" + i + "ee" + e)
    wx.showLoading({
      title: '数据同步中',
    })
    const filePath = e
    // 上传图片
    const cloudPath = 'dzimg/' + wx.getStorageSync('openid') + '/icon/' + (new Date()).valueOf() + filePath.match(/\.[^.]+?$/)[0]
    wx.cloud.uploadFile({
      cloudPath,
      filePath,
      success: res => {
        console.log('[上传文件] 成功：', res)
        let ui = this.data.diy;
        ui[this.data.show_num].icon[i].url = res.fileID

        this.setData({
          diy: ui
        })
        wx.hideLoading();
      },
      fail: e => {
        console.error('[上传文件] 失败：', e)
        wx.showToast({
          icon: 'none',
          title: '上传失败',
        })
        wx.hideLoading();
      },
      complete: () => {
      }
    })
  },
  onIconTitleChange: function (e) {
    console.log(e)
    let ui = this.data.diy;
    ui[this.data.show_num].icon[e.currentTarget.dataset.index].title = e.detail

    this.setData({
      diy: ui
    })

  },
  chooseIconEvent: function (e) {
    let index = e.currentTarget.dataset.index;
    this.setData({
      tmpIconSelectIndex: index
    })
    this.toggleRightPopup2();
  },
  onTextChange: function (e) {
    console.log(e)
    let ui = this.data.diy;
    ui[this.data.show_num].context = e.detail

    this.setData({
      diy: ui
    })

  },
  onTextSlidChange: function (e) {
    let ui = this.data.diy;
    ui[this.data.show_num].layoutLeft = e.detail;

    this.setData({
      diy: ui
    })

  },
  chooseDiyImage: function (e) {
    console.log(e)
    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        console.log(res)
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.uploadDiyImg(res.tempFilePaths[0])
      }
    })
  },
  uploadDiyImg: function (e, i) {
    console.log("qqq" + i + "ee" + e)
    wx.showLoading({
      title: '数据同步中',
    })
    const filePath = e
    // 上传图片
    const cloudPath = 'dzimg/' + wx.getStorageSync('openid') + '/image/' + (new Date()).valueOf() + filePath.match(/\.[^.]+?$/)[0]
    console.log(cloudPath)
    wx.cloud.uploadFile({
      cloudPath,
      filePath,
      success: res => {
        console.log('[上传文件] 成功：', res)
        let ui = this.data.diy;
        ui[this.data.show_num].url = res.fileID

        this.setData({
          diy: ui
        })
        wx.hideLoading();
      },
      fail: e => {
        console.error('[上传文件] 失败：', e)
        wx.showToast({
          icon: 'none',
          title: '上传失败',
        })
        wx.hideLoading();
      },
      complete: () => {
      }
    })
  },
  onGoodsChange: function (e) {
    console.log(e.detail)
    console.log(this.data.diy[this.data.show_num].product_list.length)
    let mt = { id: '1', img_url: '/images/noimg2.png', name: '商品', price: '0.2', price_org: '0.1', sale: '10', subname: '描述' }
    let menuDiy = this.data.diy
    if (parseInt(this.data.diy[this.data.show_num].product_list.length) > parseInt(e.detail)) {
      menuDiy[this.data.show_num].product_list.splice(this.data.diy[this.data.show_num].product_list.length - 1, 1)
      this.setData({
        diy: menuDiy
      })
      console.log(23)
    } else if (parseInt(this.data.diy[this.data.show_num].product_list.length) < parseInt(e.detail)) {
      console.log(345)
      menuDiy[this.data.show_num].product_list.push(mt)
      this.setData({
        diy: menuDiy
      })
    }
  },
  chooseGoodsImage: function (e) {
    let index = e.currentTarget.dataset.index;
    let that = this;
    that.setData({
      tmpGoodsImgs: index
    })
    wx.navigateTo({
      url: '../../../pages/dy/product/index',
      complete: function () {
        console.log("开始了")
      }
    })
  },
  release: function (e) {
    // db.collection('index_diy').where({
    //   type: '首页'
    // }).get().then(res => {
    //   console.log(res.data.length)
    //   if(res.data.length>0){
    //     db.collection('index_diy').doc(res.data[0]._id).remove({
    //       success(res) {
    //         console.log(res)
    //       }
    //     })
    //   }
    //     
    console.log(this.data.all)
    db.collection('index_diy').add({
      // data 字段表示需新增的 JSON 数据
      data: {
        type: '首页',
        createtime: util.formatTime(new Date()),
        all: this.data.all
      }
    })
      .then(res => {
        console.log(res)
        wx.showToast({
          title: '发布成功',
        })
        // setTimeout(function () {
        //   wx.redirectTo({
        //     url: '../../../pages/com/home/index',
        //   })
        // }, 1000)

      })


    // })

  },
  closePagesList: function (e) {
    this.toggle('right2');
  },
  addPage: function (e) {
    if (this.data.pageType == "") {
      Toast.fail('请选择页面分类');
      return;
    }
    if (this.data.tmpAddPageName == "") {
      Toast.fail('请输入页面名称');
      return;
    }
    let a = this.data.all
    a.push({ name: this.data.tmpAddPageName, type: this.data.pageType, content: [] })
    this.setData({
      all: a
    })
    this.setData({
      tmpAddPageName: '',
      pageType: ''
    })
    Toast.success('添加成功')
  },
  pageNameChange: function (e) {
    console.log(e)
    this.setData({
      tmpAddPageName: e.detail
    })
  },
  updatePage: function (e) {
    console.log(e)
    for (var i = 0; i < this.data.all.length; i++) {
      if (this.data.all[i].name == e.currentTarget.dataset.name) {
        this.setData({
          tmpCurrentPageName: e.currentTarget.dataset.name
        })
        this.getPageData()
      }
    }
    this.toggleRightPopup2()

    wx.setNavigationBarTitle({
      title: this.data.tmpCurrentPageName,
    })
  },
  delPage: function (e) {
    for (var i = 0; i < this.data.all.length; i++) {
      if (this.data.all[i].name == e.currentTarget.dataset.name) {
        let tt = this.data.all;
        tt.splice(i, 1)
        this.setData({
          all: tt
        })
      }
    }
  },
  selectPage: function (e) {
    for (var i = 0; i < this.data.all.length; i++) {
      if (this.data.all[i].name == e.currentTarget.dataset.name) {
        let ui = this.data.diy;
        ui[this.data.show_num].icon[this.data.tmpIconSelectIndex].event = e.currentTarget.dataset.name;

        this.setData({
          diy: ui
        })
      }
    }
    this.toggleRightPopup2()
  },
  goOtherPage: function (e) {
    for (var i = 0; i < this.data.all.length; i++) {
      if (this.data.all[i].name == e.currentTarget.dataset.event) {
        if (this.data.all[i].name == e.currentTarget.dataset.event) {
          if (this.data.all[i].name == '商品列表' || this.data.all[i].name == '管理中心') {
            wx.navigateTo({
              url: this.data.all[i].url
            })
          } else {
            wx.setStorageSync('tmpCurrentPageName', this.data.all[i].name)
            if (this.data.all[i].type == '主页') {
              wx.switchTab({
                url: '../../../pages/com/index/index'
              })
            } else if (this.data.all[i].type == '一级页面') {
              wx.navigateTo({
                url: '../../../pages/com/one/index'
              })
            } else if (this.data.all[i].type == '二级页面') {
              wx.navigateTo({
                url: '../../../pages/com/two/index'
              })
            } else if (this.data.all[i].type == '三级页面') {
              wx.navigateTo({
                url: '../../../pages/com/three/index'
              })
            } else {
              wx.switchTab({
                url: '../../../pages/com/index/index'
              })
            }
            // this.setData({
            //   tmpCurrentPageName: e.currentTarget.dataset.event
            // })
            // this.getPageData()
          }

        }
      }
    }
  },
  upmove: function (e) {
    let index = e.currentTarget.dataset.index;
    let ui = this.data.diy;
    let tmp = {};
    if (parseInt(index) > 0) {
      if (ui.length > 1) {
        tmp = ui[index];
        ui[index] = ui[index - 1];
        ui[index - 1] = tmp;
        this.setData({
          diy: ui
        })
      }
    }
  },
  downmove: function (e) {
    let index = e.currentTarget.dataset.index;
    let ui = this.data.diy;
    let tmp = {};
    if (ui.length > 1) {
      if (parseInt(index) != ui.length - 1) {
        tmp = ui[index];
        ui[index] = ui[index + 1];
        ui[index + 1] = tmp;
        this.setData({
          diy: ui
        })
      }
    }
  },
  addPageSelectType: function (e) {
    this.setData({
      showPageType: true
    })
  },
  onClose() {
    this.setData({ showPageType: false });
  },

  onSelect(event) {
    console.log(event.detail);
    this.setData({ pageType: event.detail.name, showPageType: false })

  },
  goProductDetail: function (e) {
    wx.navigateTo({

      url: `/pages/shop/product/product-detail/index?id=${e.currentTarget.dataset.id}`
    });
  },
  goFirst(e) {
    wx.redirectTo({
      url: '../../../pages/com/home/index',
    })
  },
  onBottomMenuChange: function (e) {
    console.log(e.detail)
    if (e.detail.index == 0) {
      this.toggleRightPopup()
    } else if (e.detail.index == 1) {
      this.toggleRightPopup2()
    } else if (e.detail.index == 2) {
      this.release()
    } else if (e.detail.index == 3) {
      this.goFirst()
    }
  },
  onClickShowPageAdd: function (e) {
    if (this.data.showPageAdd) {
      this.setData({
        showPageAdd: false
      })
    } else {
      this.setData({
        showPageAdd: true
      })
    }
  }
});
