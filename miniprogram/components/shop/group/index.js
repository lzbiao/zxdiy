const util = require("../../../utils/util.js");

Component({
    properties: {
        groupData: Object
    },
    data: {
        defaultWidth: util.getSystem().windowWidth,  // 微信浏览器宽
    },
    attached: function () {
        // console.log(this);
    },
    methods: {
        goDetail(e) {
            wx.navigateTo({
                url: `/pages/product/index?detail_id=${e.currentTarget.dataset.item.id}&type=${e.currentTarget.dataset.item.product_type}`
            });
        }
    }
});